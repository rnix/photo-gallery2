Simple scripts for demonstation photo
=====================================

 
* Photos should be placed in public/gallery 
* Thumbs (the same file names) should be placed in public/gallery/s
* Titles can be specified in public/gallery/_descr.txt in following format:

```
#!

  img_file_name1.ext#Title1
  img_file_name2.ext#Ttile2
  ...
  img_file_nameN.ext#TtileN
```