<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>PhotoGallery</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <link href="/fancybox/jquery.fancybox-1.3.4.css" rel="stylesheet">
        <link href="/css/style.css" rel="stylesheet">
        <link rel="shortcut icon" href="/favicon.ico" />

        <!--[if lt IE 9]>
          <script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
    </head>

    <body>
        <div class="central-content">
            <div>
                {if isset($content)}{$content}{else}no content{/if}
            </div>
        </div>

    {literal}
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>

        <script src="/fancybox/jquery.mousewheel-3.0.4.pack.js"></script>
        <script src="/fancybox/jquery.fancybox-1.3.4.pack.js"></script>

        <script>
            jQuery(document).ready(function() {
                $(".gallery a").fancybox();
            });
        </script>
    {/literal}
</body>
</html>
