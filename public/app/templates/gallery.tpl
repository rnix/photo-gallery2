{if $photos}
    <p class="gallery">
        {strip}
            {foreach from=$photos item=i}
                <a href="/gallery/{$i.filename}" {if isset($i.title)}title="{$i.title}"{/if} rel="photo_group">
                    <img src="/gallery/s/{$i.filename}" alt="" {if isset($i.id)}data-photoId="{$i.id}"{/if}>
                </a>
            {/foreach}
        {/strip}
    </p>
{else}
    <p>Фотографий нет</p>
{/if}