﻿<?php

class Application {

    public $smarty;
    protected $_descrFile;

    public function run() {
        $this->_init();


        $dir = APPLICATION_PATH . "/../gallery";

        $images = glob($dir . "/*.[jJ][pP][gG]");
        $photos = array();
        $descriptions = $this->_loadDescriptions();
        foreach ($images as $imgfile) {
            $filename = basename($imgfile);
            $photo = array(
                'filename' => $filename,
            );
            if (array_key_exists($filename, $descriptions)) {
                $photo['title'] = $descriptions[$filename];
            }
            $photos[] = $photo;
        }
        $this->smarty->assign('photos', $photos);
        $cnt = $this->smarty->fetch('gallery.tpl');
        $this->smarty->assign('content', $cnt);

        $this->smarty->display('layout.tpl');
    }

    protected function _loadDescriptions() {
        $descriptions = array();
        if (is_readable($this->_descrFile)) {
            $handle = fopen($this->_descrFile, "r");
            if ($handle !== false) {
                while (($data = fgetcsv($handle, 5000, "#")) !== false) {
                    if (isset($data[1])) {
                        $descriptions[$data[0]] = $data[1];
                    }
                }
                fclose($handle);
            }
        }
        return $descriptions;
    }

    protected function _init() {
        defined('APPLICATION_ENV') || define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'production'));

        // Define path to application directory
        defined('APPLICATION_PATH') || define('APPLICATION_PATH', realpath(dirname(__FILE__)));

        set_include_path(implode(PATH_SEPARATOR, array(
            realpath(APPLICATION_PATH),
            get_include_path(),
        )));

        $this->_descrFile = APPLICATION_PATH . "/../gallery/_descr.txt";

        require('smarty/Smarty.class.php');
        $smarty = new Smarty;
        $smarty->template_dir = APPLICATION_PATH . '/templates';
        $smarty->compile_dir = APPLICATION_PATH . '/templates/_c';
        $this->smarty = $smarty;
    }

}
